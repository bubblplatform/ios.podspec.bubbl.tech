# Bubbl Plugin Podspec - Cocoapod

Use the `ios-plugin-deploy-to-cocoapods.sh` in the Bubbl tools repository (https://bitbucket.org/bubblplatform/tools.bubbl.tech/) to deploy the BubblPlugin to CocoaPods.

```
cd ios
. ios-plugin-deploy-to-cocoapods.sh <plugin-version>
```

